<?php
//Start Session
session_start();

//Include config
require 'config.php';

//Load core Classes
require 'classes/Bootstrap.php';
require 'classes/Controller.php';
require 'classes/Model.php';
require 'classes/Messages.php';

//Load Controllers
require 'controllers/Home.php';
require 'controllers/Shares.php';
require 'controllers/Users.php';

//Load Models
require 'models/Home.php';
require 'models/Share.php';
require 'models/User.php';

$bootstrap = new Bootstrap($_GET);

$controller = $bootstrap->createController();

if ($controller) {
    $controller->executeAction();
}
