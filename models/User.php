<?php

/**
 * User Model
 */
class UserModel extends Model
{
    public function register()
    {
        //Sanatize the POST array
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if ($post['submit']) {

            if ($post['user_name'] == '' || $post['user_email'] == '' || $post['user_password'] == '') {
                Messages::setMessage('Please fill the fields', 'error');
                return;
            }

            $password = md5($post['user_password']);

            //Insert into mySql
            $this->query('INSERT INTO users (name,email,password) VALUES(:name,:email,:password)');
            $this->bind(':name', $post['user_name']);
            $this->bind(':email', $post['user_email']);
            $this->bind(':password', $password);
            $this->execute();
            //Verify
            if ($this->lastInsertId()) {
                Messages::setMessage('The Account Was Successfully Created', 'success');
                //Redirect
                header('Location: ' . ROOT_URL . 'users/login');
            }
        }

        return;
    }

    public function login()
    {
        //Compare Login
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if ($post['login']) {

            $password = md5($post['user_password']);

            $this->query('SELECT * FROM users WHERE email = :email AND password = :password');
            $this->bind(':email', $post['user_name']);
            $this->bind(':password', $password);

            $row = $this->single();

            //Verify
            if ($row) {
                $_SESSION['is_logged_in'] = true;
                $_SESSION['user_data'] = array(
                    'id' => $row['id'],
                    'name' => $row['name'],
                    'email' => $row['email'],
                );
                header('Location: ' . ROOT_URL . 'shares');
            } else {
                Messages::setMessage('Incorrect Login', 'error');
            }
        }

        return;
    }
}
