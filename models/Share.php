<?php

/**
 * Share Model
 */
class ShareModel extends Model
{
    public function Index()
    {
        $user_id = (int) $_SESSION['user_data']['id'];
        $this->query('SELECT * FROM shares WHERE user_id = ' . $user_id . ' ORDER BY create_date DESC');
        $rows = $this->resultSet();
        return $rows;
    }

    public function add()
    {
        //Sanatize the POST array
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if ($post['submit']) {

            $user_id = (int) $_SESSION['user_data']['id'];

            //Insert into mySql
            $this->query('INSERT INTO shares (title,body,link,user_id) VALUES(:title,:body,:link,:user_id)');
            $this->bind(':title', $post['share_title']);
            $this->bind(':body', $post['share_body']);
            $this->bind(':link', $post['share_link']);
            $this->bind(':user_id', $user_id);
            $this->execute();
            //Verify
            if ($this->lastInsertId()) {
                //Redirect
                header('Location: ' . ROOT_URL . 'shares');
            }
        }

        return;
    }
}
