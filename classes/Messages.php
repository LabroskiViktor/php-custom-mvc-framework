<?php

/**
 * Messages class
 */
class Messages
{
    public static function setMessage($text, $type)
    {
        if ($type == 'error') {
            $_SESSION['error_message'] = $text;
        }

        if ($type == 'success') {
            $_SESSION['success_message'] = $text;
        }
    }

    public static function displayMessage()
    {
        if (isset($_SESSION['error_message'])) {
            echo '<div class="alert alert-danger text-center">' . $_SESSION['error_message'] . '</div>';
            unset($_SESSION['error_message']);
        }

        if (isset($_SESSION['success_message'])) {
            echo '<div class="alert alert-success text-center">' . $_SESSION['success_message'] . '</div>';
            unset($_SESSION['success_message']);
        }
    }

}
