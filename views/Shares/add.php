<div class="col-md-12">
	<h1>Add New Share</h1>
	<form method="POST" action="<?php $_SERVER['PHP_SELF']; ?>">
		<div class="form-group">
			<label for="share_title">Share Title</label>
			<input type="text" name="share_title" id="share_title" class="form-control">
		</div>
		<div class="form-group">
			<label for="share_body">Share Body</label>
			<textarea class="form-control" name="share_body" id="share_body"></textarea>
		</div>
		<div class="form-group">
			<label for="share_link">Link</label>
			<input type="text" name="share_link" id="share_link" class="form-control">
		</div>
		<input type="submit" name="submit" class="btn btn-info" value="Sumbit">
		<a href="<?= ROOT_PATH; ?>shares" class="btn btn-danger">Cancel</a>
	</form>
</div>