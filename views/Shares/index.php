<?php if(isset($_SESSION['is_logged_in']))  : ?>
	<div class="col-md-12">
		
		<a class="btn btn-success btn-share" href="<?php ROOT_PATH; ?>shares/add">Share something</a>
		<br><br>
		
		<?php foreach ($viewModel as $item)  :?>
		<div class="card">
			<div class="card-body">
				<h3><?= $item['title'] ?> <small><?= $item['create_date'] ?></small></h3>
				<p><?= $item['body']  ?></p>
			</div>
		</div>
		<br>
		<?php endforeach; ?>
		
	</div>
<?php else: header('Location: ' . ROOT_URL . 'users/login'); ?>
<?php endif; ?>