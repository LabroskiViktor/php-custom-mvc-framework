<!DOCTYPE html>
<html>
	<head>
		<title>Shareboard</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
		<style><?php require 'assets/css/style.css'; ?></style>
	</head>
	<body>
		<nav class="navbar navbar-expand-sm navbar-light bg-secondary">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="<?= ROOT_URL ?>">Home</a>
				</li>
				<?php if(isset($_SESSION['is_logged_in']))  : ?>
					<li class="nav-item">
						<a class="nav-link" href="<?= ROOT_URL ?>shares">Shares</a>
					</li>
				<?php endif; ?>
			</ul>
			<ul class="navbar-nav ml-auto">
				<?php if(isset($_SESSION['is_logged_in']))  : ?>
					<li class="nav-item">
						<a class="nav-link" href="<?= ROOT_URL ?>">Welcome <?= $_SESSION['user_data']['name'] ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= ROOT_URL ?>users/logout">Log Out</a>
					</li>
				<?php else : ?>
					<li class="nav-item">
						<a class="nav-link" href="<?= ROOT_URL ?>users/login">Login</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= ROOT_URL ?>users/register">Register</a>
					</li>
				<?php endif; ?>
			</ul>
		</nav>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php Messages::displayMessage() ?>
				</div>
				<?php require($view); ?>
			</div>
		</div>
	</body>
</html>