<div class="col-md-12">
	<h1>Login</h1>
	<form method="POST" action="<?php $_SERVER['PHP_SELF']; ?>">
		<div class="form-group">
			<label for="user_name">User Name</label>
			<input type="text" name="user_name" class="form-control">
		</div>
		<div class="form-group">
			<label for="user_password">Password</label>
			<input type="password" name="user_password" class="form-control">
		</div>
		<input type="submit" name="login" value="Login" class="btn btn-info">
	</form>
</div>