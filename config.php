<?php

//Define DB params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'password');
define('DB_NAME', 'shareboard');

//Define URL
define('ROOT_PATH', 'http://localhost/custom-mvc/');
define('ROOT_URL', 'http://localhost/custom-mvc/');
