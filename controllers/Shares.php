<?php

/**
 * Shares controller
 */
class Shares extends Controller
{

    protected function index()
    {
        $viewmodel = new ShareModel();
        $this->ReturnView($viewmodel->Index(), true);
    }

    public function add()
    {
        if (!isset($_SESSION['is_logged_in'])) {
            header('Location: ' . ROOT_URL . 'users/login');
        } else {
            $viewmodel = new ShareModel();
            $this->ReturnView($viewmodel->add(), true);
        }
    }
}
